package com.ims.service;

import com.ims.model.Customer;
import com.ims.model.Product;
import com.ims.repository.CustomerRepository;
import com.ims.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;

    public ProductService(ProductRepository productRepository, CustomerRepository customerRepository) {
        this.productRepository = productRepository;
        this.customerRepository = customerRepository;
    }

    public List<Product> searchProducts(String productKeyword, String customerInfo) {
        // TODO handle the case http://localhost:8080/api/v1/products/search?query=je&customer-info=jo
        if (productKeyword != null) {
            return productRepository.searchProductsByKeyword(productKeyword);
        }
        if (customerInfo != null) {
            return searchProductsByCustomer(customerInfo);
        }

        // TODO what should we do when both productKeyword and customerInfo are null
        return productRepository.findAll();
    }

    public List<Product> searchProductsByCustomer(String customerInfo){
        if (customerInfo != null) {
            List<Long> productIds = customerRepository.searchCustomers(customerInfo).stream()
                    .map(Customer::getId)
                    .map(productRepository::findAllByCustomerId)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());

            return productIds.stream()
                    .map(id -> productRepository
                            .findById(id)
                            // TODO define not found exception
                            .orElseThrow(() -> new IllegalArgumentException("foo")))
                    .collect(Collectors.toList());
        }
        return List.of();
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Optional<Product> findById(Long id) { return productRepository.findById(id); }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public void remove(Long id) {
        productRepository.deleteById(id);
    }

}
