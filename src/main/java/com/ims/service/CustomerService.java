package com.ims.service;

import com.ims.model.Customer;
import com.ims.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private final CustomerRepository  customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> searchCustomers(String keyword) {
        if (keyword != null) {
            return customerRepository.searchCustomers(keyword);
        }
        return customerRepository.findAll();
    }

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public Optional<Customer> findById(Long id) { return customerRepository.findById(id); }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public void remove(Long id) {
        customerRepository.deleteById(id);
    }

}
