package com.ims.repository;

import com.ims.model.Customer;
import com.ims.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(nativeQuery = true, value = "select * from Customer c order by c.id desc limit 1")
    Product findLastCreatedCustomer(Long id);

    @Query(nativeQuery = true, value = "SELECT * FROM Customer c WHERE lower(CONCAT(c.first_name, ' ', c.last_name, ' ', c.email, ' ', c.phone)) LIKE lower(concat('%', ?1,'%'))")
    List<Customer> searchCustomers(String keyword);
}
