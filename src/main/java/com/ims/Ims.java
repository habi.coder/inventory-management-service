package com.ims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ims {

    public static void main(String[] args) {
        SpringApplication.run(Ims.class, args);
    }

}
