-- DELETE FROM product;
-- DELETE FROM customer;
-- DELETE FROM image;
-- DELETE FROM product_image_relation;
--
-- ---
-- INSERT INTO product (id, name, category, price, images)
-- VALUES
--     (1, 'Tom', 'cat', 1, 'this is an url'),
--     (2, 'Jerry', 'mouse', 2, 'this is an url');
-- ---
-- INSERT INTO customer (id, first_name, last_name, email, phone, notes)
-- VALUES
--         (1, 'Joe', 'Doe', 'joe.doe@mail.com', '', '' ),
--         (2, 'Jil', 'Doe', 'jil.doe@mail.com', '', '');
-- ---
-- INSERT INTO image (id, url, uri)
-- VALUES
--        (1, 'http://url/1', 's3://bucket-name/object-name-1'),
--        (2, 'http://url/2', 's3://bucket-name/object-name-2');
-- ---
-- INSERT INTO product_image_relation (id, image_id, product_id)
-- VALUES
--        (1, 1, 2),
--        (2, 2, 1);
-- ---
-- UPDATE product
-- SET customer_id = 1
-- WHERE id = 1;
--
-- UPDATE product
-- SET customer_id = 1
-- WHERE id = 2;
