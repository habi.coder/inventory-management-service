package com.ims.controller;

import com.ims.model.Product;
import com.ims.repository.ProductRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Disabled
class ProductControllerTest {

    @Autowired
    ProductRepository pRepo;

    @Test
    @Order(1)
    void createNewProduct() {
        Product p = new Product();
        p.setName("Iphone12");
        p.setCategory("smartphone");
        p.setPrice(3F);
        pRepo.save(p);
        System.out.println(pRepo.findAll());
        assertEquals(p, pRepo.findLastCreatedProduct(p.getId()));
    }

    @Test
    @Order(2)
    void getAllProduct() {
        List<Product> list = pRepo.findAll();
        System.out.println(pRepo.findAll());
        Assertions.assertThat(list).size().isGreaterThan(0);
    }

    @Test
    @Order(3)
    void getProduct() {
        Product p = new Product();
        p = pRepo.findLastCreatedProduct(p.getId());
        System.out.println(p);
        assertEquals(p, pRepo.findLastCreatedProduct(p.getId()));
    }

    @Test
    @Order(4)
    void updateProduct() {
        Product p = new Product();
        p = pRepo.findLastCreatedProduct(p.getId());
        p.setCategory("updated descriptions");
        pRepo.save(p);
        System.out.println(p);
        assertEquals("updated descriptions", pRepo.findLastCreatedProduct(p.getId()).getCategory());
    }

    @Test
    @Order(5)
    void deleteProduct() {
        Product p = new Product();
        p = pRepo.findLastCreatedProduct(p.getId());
        pRepo.delete(p);
        System.out.println(pRepo.findAll());
        Assertions.assertThat(pRepo.existsById(p.getId())).isFalse();
    }
}
